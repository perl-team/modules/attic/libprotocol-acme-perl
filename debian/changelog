libprotocol-acme-perl (1.02-1) UNRELEASED; urgency=medium

  NOTE:
  [Deprecated] This module is no longer maintained. My efforts have gone
  towards using and supporting L<Net::ACME2>.

  [ Salvatore Bonaccorso ]
  * Import upstream version 1.02

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.1.5, no changes needed.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 08 Sep 2019 15:14:59 +0200

libprotocol-acme-perl (1.01-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Only remove top-level README.pod from the binary package if it exists
    (Closes: #899017)
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.4
  * Declare that the package does not need (fake)root to build

 -- Niko Tyni <ntyni@debian.org>  Fri, 18 May 2018 17:49:27 +0300

libprotocol-acme-perl (1.01-2) unstable; urgency=medium

  * Upload to unstable

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 01 Nov 2016 21:25:45 +0100

libprotocol-acme-perl (1.01-1) experimental; urgency=medium

  * Team upload.
  * Import upstream version 1.01.
  * Refresh fix-spelling-error.patch (offset).
  * Update long description.
  * Add build dependency on openssl to enable more tests.

 -- gregor herrmann <gregoa@debian.org>  Mon, 31 Oct 2016 19:26:10 +0100

libprotocol-acme-perl (0.16-1) experimental; urgency=medium

  * Import upstream version 0.16

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 13 Aug 2016 19:34:55 +0200

libprotocol-acme-perl (0.15-1) experimental; urgency=medium

  * Import upstream version 0.14 and 0.15
  * Drop spelling-error-in-manpage.patch patch
  * Properly install provided examples files.
    Upstream ship them now in a separate directory so we can properly
    install them. Remove previous workaround/override for dh_auto_install to
    remove the installed files in /usr/share/perl5/Protocol/.
  * Skip tests which need network access both in testsuite and autopkgtests.
    Rework how we do that. Upstream has patched the tests to respect
    NO_NETWORK environment variable. Set this to run the testsuite and set
    it as well in the environment for the autopkgtests. Drop unneeded
    patch to make NO_NETWORK available in t/04-load_key_crypt.t.
  * Add fix-spelling-error.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 21 Jul 2016 22:05:46 +0200

libprotocol-acme-perl (0.12-1) experimental; urgency=low

  * Initial Release (Closes: #813556).

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 06 Jul 2016 14:14:27 +0200
